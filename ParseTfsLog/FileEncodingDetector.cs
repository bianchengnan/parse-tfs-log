using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

public static class TextFileEncodingDetector
{
    public static Encoding GetEncoding(string InputFilename)
    {
        using (FileStream textfileStream = File.OpenRead(InputFilename))
        {
            return GetEncoding(textfileStream);
        }
    }

    public static Encoding GetEncoding(FileStream textfileStream)
    {
        if (textfileStream == null)
            throw new ArgumentNullException("Must provide a valid file stream!", "InputFileStream");

        if (!textfileStream.CanRead)
            throw new ArgumentException("Provided file stream is not readable!", "InputFileStream");

        if (!textfileStream.CanSeek)
            throw new ArgumentException("Provided file stream cannot seek!", "InputFileStream");

        byte[] sampleBytes = new byte[textfileStream.Length];
        textfileStream.Read(sampleBytes, 0, sampleBytes.Length);
        return GetEncoding(sampleBytes);
    }

    public static Encoding GetEncoding(byte[] sampleBytes)
    {
        var detector = new TextEncodingDetect();
        return ToSystemEncoding(detector.DetectEncoding(sampleBytes, sampleBytes.Length));
    }

    public static System.Text.Encoding ToSystemEncoding(TextEncodingDetect.Encoding value)
    {
        switch (value)
        {
            case TextEncodingDetect.Encoding.Ascii:
                return System.Text.Encoding.ASCII; 
            case TextEncodingDetect.Encoding.Utf8Bom:
            case TextEncodingDetect.Encoding.Utf8NoBom:
                return System.Text.Encoding.UTF8; 
            case TextEncodingDetect.Encoding.Utf16LeBom:
            case TextEncodingDetect.Encoding.Utf16LeNoBom:
                return System.Text.Encoding.Unicode;
            case TextEncodingDetect.Encoding.Utf16BeBom:
            case TextEncodingDetect.Encoding.Utf16BeNoBom:
                return System.Text.Encoding.BigEndianUnicode;
            default:
                return System.Text.Encoding.Default;
        }
    }
}

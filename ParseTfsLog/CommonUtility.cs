﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
public class CommonUtility
{
    public static int FirstNotOf(string value, char ch)
    {
        for (int idx = 0; idx < value.Length; ++idx)
        {
            if (ch != value[idx])
            {
                return idx;
            }
        }

        return -1;
    }

    public static int FirstNotOf(string value, char[] chs)
    {
        for (int idx = 0; idx < value.Length; ++idx)
        {
            if (!chs.Contains(value[idx]))
            {
                return idx;
            }
        }

        return -1;
    }

    public static string Trim(string value)
    {
        char[] trimChars = { ' ', '\t', '、', '\n', '\r' };
        return value.Trim(trimChars);
    }

    public static void Trim(ref List<string> valueList)
    {
        for (int idx = 0; idx < valueList.Count; ++idx)
        {
            valueList[idx] = Trim(valueList[idx]);
        }
    }

    public static bool IsCommentLine(string value)
    {
        var trimmedValue = Trim(value);
        return trimmedValue.StartsWith("//")
            || trimmedValue.StartsWith(";")
            || trimmedValue.StartsWith("#");
    }

    public static void Unique(ref List<string> dataList)
    {
        dataList = dataList.Distinct().ToList();
    }

    public static void RemoveEmpty(ref List<string> dataList)
    {
        dataList.RemoveAll(data => { return string.IsNullOrEmpty(data); });
    }

    public static void RemoveEmptyAndUnique(ref List<string> dataList)
    {
        RemoveEmpty(ref dataList);
        Unique(ref dataList);
    }

    public static List<string> Split(string data)
    {
        return Split(data, ',');
    }

    public static List<string> Split(string data, char ch)
    {
        char[] splitChars = { ch };
        return Split(data, splitChars);
    }

    public static List<string> Split(string data, char[] splitChars)
    {
        var result = data.Split(splitChars).ToList();
        Trim(ref result);
        RemoveEmpty(ref result);
        return result;
    }

    public static String WildCardToRegex(string rex)
    {
        return Regex.Escape(rex).Replace("\\?", ".").Replace("\\*", ".*");
    }

    public static string GetMatchedValue(MatchCollection matches, int index = 1)
    {
        if (matches.Count <= 0)
        {
            return "";
        }

        return matches[0].Groups[index].Value;
    }

    public static List<string> LoadFileByLine(string path, Encoding encoding)
    {
        if (string.IsNullOrEmpty(path))
        {
            return new List<string>();
        }

        if (!System.IO.File.Exists(path))
        {
            System.Console.WriteLine("File [{0}] not existed.", path);
            return new List<string>();
        }

        List<string> result = new List<string>();

        FileStream fileStream = null;
        StreamReader streamReader = null;

        try
        {
            fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            streamReader = new StreamReader(fileStream, encoding);
            string curLine = streamReader.ReadLine();
            while (curLine != null)
            {
                result.Add(curLine);
                curLine = streamReader.ReadLine();
            }
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.Fail(e.ToString());
        }
        finally
        {
            if (streamReader != null)
            {
                streamReader.Close();
            }

            if (fileStream != null)
            {
                fileStream.Close();
            }
        }

        return result;
    }

    public static bool SaveToFile(string path, Encoding encoding, List<string> dataList)
    {
        FileStream fileStream = null;
        StreamWriter streamWriter = null;

        try
        {
            FileAttributes originalAttribute = FileAttributes.Normal;
            bool bExisted = System.IO.File.Exists(path);
            if (bExisted)
            {
                originalAttribute = File.GetAttributes(path);
                var attr = originalAttribute & ~FileAttributes.ReadOnly; // remove ReadOnly attribute.
                File.SetAttributes(path, attr);

                fileStream = new FileStream(path, FileMode.Truncate, FileAccess.ReadWrite);
            }
            else
            {
                fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            }

            streamWriter = new StreamWriter(fileStream, encoding);

            foreach (var curLine in dataList)
            {
                streamWriter.WriteLine(curLine);
            }

            if (bExisted)
            {
                File.SetAttributes(path, originalAttribute);
            }

            return true;
        }
        catch (Exception e)
        {
            System.Diagnostics.Debug.Fail(e.ToString());
        }
        finally
        {
            if (streamWriter != null)
            {
                streamWriter.Close();
            }

            if (fileStream != null)
            {
                fileStream.Close();
            }
        }

        return false;
    }

    public static string GetExePath()
    {
        return System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
    }
}

public struct ItemRange
{
    public int Start;
    public int End;   // 指向结尾下一个，类似 stl 中的 end

    public ItemRange(int start = -1, int end = -1)
    {
        Start = start;
        End = end;
    }

    public int Length()
    {
        return End - Start;
    }

    public bool IsValid()
    {
        return End > Start;
    }

    public override string ToString()
    {
        return string.Format("start: {0}, end: {1}, length: {2}", Start, End, Length());
    }
}

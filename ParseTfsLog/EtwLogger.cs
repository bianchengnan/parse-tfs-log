﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Tracing;

public class EtwLogger : EventSource
{
    public static EtwLogger Instance = new EtwLogger();

    public class Keywords
    {
        public const EventKeywords Loop = (EventKeywords)1;
        public const EventKeywords MethodBegin = (EventKeywords)2;
        public const EventKeywords MethodEnd = (EventKeywords)4;
        public const EventKeywords Message = (EventKeywords)8;
    }

    [Event(1, Level = EventLevel.Verbose, Keywords = Keywords.Loop, Message = "Loop {0} iteration {1}")]
    public void LoopIteration(string loopTitle, int iteration)
    {
        WriteEvent(1, loopTitle, iteration);
    }

    [Event(2, Level = EventLevel.Informational, Keywords = Keywords.Loop,
    Message = "Loop {0} begin")]
    public void LoopBegin(string loopTitle)
    {
        WriteEvent(2, loopTitle);
    }

    [Event(3, Level = EventLevel.Informational, Keywords = Keywords.Loop,
    Message = "Loop {0} done")]
    public void LoopDone(string loopTitle)
    {
        WriteEvent(3, loopTitle);
    }

    [Event(4, Level = EventLevel.Informational, Keywords = Keywords.MethodBegin)]
    public void MethodBegin(string methodName)
    {
        WriteEvent(4, methodName);
    }

    [Event(5, Level = EventLevel.Informational, Keywords = Keywords.MethodEnd)]
    public void MethodEnd(string methodName)
    {
        WriteEvent(5, methodName);
    }

    [Event(6, Level = EventLevel.Informational, Keywords = Keywords.Message, Message = "{0}")]
    public void TraceMessage(string message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
    {
        var msg = string.Format("{0}, [{1}], [{2}:{3}]", message, memberName, sourceFilePath, sourceLineNumber);
        WriteEvent(6, msg);
    }
}

public class AutoScopeLogger : IDisposable
{
    public AutoScopeLogger(string scopeName)
    {
        scope_ = scopeName;
        EtwLogger.Instance.MethodBegin(string.Format("[+] {0} start", scope_));
    }

    public void Dispose()
    {
        EtwLogger.Instance.MethodEnd(string.Format("[+] {0} end", scope_));
    }

    private string scope_ = "";
}
